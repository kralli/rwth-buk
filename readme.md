# Ausführung

1. Im Projekt `./mvnw clean install` (Unix) oder `mvnw.cmd clean install` (Windows) ausführen.
2. Mit Java 8+ `java -cp {path-to-project}/target/classes/ de.rwth.tm.Main <file> <word> [ true ]` ausführen. Java erfordert absolute Pfade für `-cp`.
   Mit `true` als drittes Argument wird eine andere Formatierung der Ausgabe verwendet.

In `StandardTuringMachine.execute(Word)` befindet sich die Ausführung.

# Funktionsweise

Zum Beginn der Ausführung wird das Eingabewort geschrieben und der Kopf an den Anfang des Wortes bewegt.

Die Ausführung läuft so lange, bis der Zustand gleich dem Endzustand ist. In jeden Schritt wird das Symbol vom Band gelesen. Das Symbol wird zusammen mit dem
Zustand in die Übergangsfunktion gegeben. Das Resultat der Übergangsfunktion wird dann angewendet. Dafür wird Zustand auf den Zustand des Ergebnisses gesetzt,
das zurückgegebene Symbol geschrieben und abschließend der Kopf entsprechend bewegt.

Zum Abschluss der Ausführung wird das Ergebniswort vom Band gelesen.

# Beispielprotokoll

```
java -cp ... de.rwth.tm.Main .../invert.TM  001101
B[1]001101B
B1[1]01101B
B11[1]1101B
B110[1]101B
B1100[1]01B
B11001[1]1B
B110010[1]B
B11001[2]0B
B1100[2]10B
B110[2]010B
B11[2]0010B
B1[2]10010B
B[2]110010B
[2]B110010B
B[3]110010B
Result: 110010
```
