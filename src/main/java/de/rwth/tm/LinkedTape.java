package de.rwth.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class LinkedTape extends AbstractTape {

    private final @NotNull Alphabet alphabet;

    public LinkedTape(@NotNull Alphabet alphabet) {
        if (!alphabet.contains(Symbol.BLANK)) {
            throw new IllegalArgumentException("tape alphabet must contain blank: " + Symbol.BLANK + " ∉ " + alphabet);
        }

        this.alphabet = alphabet;
    }

    private @Nullable Node first;
    private final @NotNull Node initial = new Node(0);
    private @Nullable Node last;

    private @NotNull Node previousNode(@NotNull Node node) {
        Node previous = node.previous;
        if (previous == null) {
            previous = new Node(node.index - 1);
            node.previous = previous;
            previous.next = node;
        }
        return previous;
    }

    private @NotNull Node nextNode(@NotNull Node node) {
        Node next = node.next;
        if (next == null) {
            next = new Node(node.index + 1);
            next.previous = node;
            node.next = next;
        }
        return next;
    }

    private @NotNull Node getNode(int index) {
        Node candidate = initial;
        while (index != candidate.index) {
            if (index < candidate.index) {
                candidate = previousNode(candidate);
            } else {
                candidate = nextNode(candidate);
            }
        }
        return candidate;
    }

    private void updateLimitations(@NotNull Node node) {
        Node lowest = first == null || node.index < first.index ? node : first;
        Node highest = last == null || node.index > last.index ? node : last;

        while (lowest.index < highest.index && lowest.symbol == Symbol.BLANK) {
            lowest = nextNode(lowest);
        }
        while (highest.index > lowest.index && highest.symbol == Symbol.BLANK) {
            highest = previousNode(highest);
        }

        first = lowest.symbol != Symbol.BLANK ? lowest : null;
        last = highest.symbol != Symbol.BLANK ? highest : null;
    }

    private static final class Node {

        final int index;

        @NotNull Symbol symbol = Symbol.BLANK;

        @Nullable Node previous;
        @Nullable Node next;

        public Node(int index) {
            this.index = index;
        }

        @Override
        public String toString() {
            return String.valueOf(previous != null ? previous.symbol : "_") + symbol + (next != null ? next.symbol : "_");
        }
    }

    @Override
    public int getFirstIndex() {
        Node first = this.first;
        return first != null ? first.index : -1;
    }

    @Override
    public int getLastIndex() {
        Node last = this.last;
        return last != null ? last.index + 1 : -1;
    }

    @Override
    public int size() {
        if (last != null && first != null) {
            return last.index - first.index;
        } else {
            return 0;
        }
    }

    @Override
    public @NotNull TapeIterator iterator(int index) {
        return new LinkedTapeIterator(getNode(index));
    }

    private class LinkedTapeIterator implements TapeIterator {

        private @NotNull Node current;

        public LinkedTapeIterator(@NotNull Node current) {
            this.current = current;
        }

        @Override
        public @NotNull Symbol previous() {
            Symbol value = get();

            current = previousNode(current);

            return value;
        }

        @Override
        public @NotNull Symbol next() {
            Symbol value = get();

            current = nextNode(current);

            return value;
        }

        @Override
        public @NotNull Symbol get() {
            return current.symbol;
        }

        @Override
        public void set(@NotNull Symbol symbol) {
            if (!alphabet.contains(symbol)) {
                throw new IllegalArgumentException("symbol is not contained in tape alphabet: " + symbol + " ∉ " + alphabet);
            }
            if (symbol != current.symbol) {
                current.symbol = symbol;

                updateLimitations(current);
            }
        }

        @Override
        public int index() {
            return current.index;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Symbol.BLANK);
        Node current = first;
        if (current != null) {
            do {
                builder.append(current.symbol);
                current = current.next;
            } while (current != null && current.previous != last);
        }
        builder.append(Symbol.BLANK);
        return builder.toString();
    }
}
