package de.rwth.tm;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import org.jetbrains.annotations.NotNull;

public final class State implements Comparable<State> {

    private static final Map<Integer, State> STATES = new ConcurrentHashMap<>();

    public static @NotNull State create(int ordinal) {
        return STATES.computeIfAbsent(ordinal, State::new);
    }

    private final int ordinal;

    private State(int ordinal) {
        if (ordinal < 0) {
            throw new IllegalArgumentException("ordinal must be equal or greater than 0");
        }
        this.ordinal = ordinal;
    }

    @Override
    public int compareTo(@NotNull State o) {
        return Integer.compare(ordinal, o.ordinal);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof State)) {
            return false;
        }
        State state = (State) o;
        return ordinal == state.ordinal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ordinal);
    }

    @Override
    public String toString() {
        return String.valueOf(ordinal);
    }
}
