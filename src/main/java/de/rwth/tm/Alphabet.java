package de.rwth.tm;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;

public final class Alphabet implements Iterable<Symbol> {

    private final @NotNull Set<Symbol> symbols;

    public Alphabet(@NotNull Symbol... symbols) {
        this(Arrays.asList(symbols));
    }

    public Alphabet(@NotNull Collection<Symbol> symbols) {
        this.symbols = Collections.unmodifiableSet(new TreeSet<>(symbols));
    }

    public boolean contains(@NotNull Symbol symbol) {
        return symbols.contains(symbol);
    }

    public boolean contains(@NotNull Alphabet alphabet) {
        return symbols.containsAll(alphabet.symbols);
    }

    @Override
    public @NotNull Iterator<Symbol> iterator() {
        return symbols.iterator();
    }

    public int size() {
        return symbols.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Alphabet)) {
            return false;
        }
        Alphabet alphabet = (Alphabet) o;
        return symbols.equals(alphabet.symbols);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbols);
    }

    @Override
    public String toString() {
        return symbols.stream().map(Symbol::toString).collect(Collectors.joining(", ", "{ ", " }"));
    }
}
