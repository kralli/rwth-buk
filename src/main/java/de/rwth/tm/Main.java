package de.rwth.tm;

import de.rwth.tm.standard.StandardParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("missing turing machine file");
            System.exit(1);
            return;
        }

        if (args.length < 2) {
            System.err.println("missing input word");
            System.exit(2);
            return;
        }

        TuringMachine turingMachine = parseTuringMachine(new File(args[0]));

        Word input = parseWord(args[1]);

        boolean pretty = args.length> 2 && Boolean.parseBoolean(args[2]);

        Word output = turingMachine.execute(input, pretty);

        System.out.println("Result: " + output);
    }

    private static @NotNull TuringMachine parseTuringMachine(@NotNull File file) throws IOException {
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
            return StandardParser.parse(inputStream);
        }
    }

    private static @NotNull Word parseWord(@NotNull String value) {
        List<Symbol> symbols = new ArrayList<>();

        for (char c : value.toCharArray()) {
            Symbol symbol = Symbol.create(String.valueOf(c));

            symbols.add(symbol);
        }

        return new Word(symbols);
    }
}
