package de.rwth.tm;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import org.jetbrains.annotations.NotNull;

public final class Symbol implements Comparable<Symbol> {

    private static final Map<String, Symbol> SYMBOLS = new ConcurrentHashMap<>();

    public static @NotNull Symbol create(@NotNull String value) {
        return SYMBOLS.computeIfAbsent(value, Symbol::new);
    }

    public static final Symbol BLANK = Symbol.create("B");

    private final @NotNull String value;

    private Symbol(@NotNull String value) {
        if (value.length() != 1) {
            throw new IllegalArgumentException("symbol must be of length 1");
        }
        this.value = value;
    }

    @Override
    public int compareTo(@NotNull Symbol other) {
        return value.compareTo(other.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Symbol)) {
            return false;
        }
        Symbol symbol = (Symbol) o;
        return value.equals(symbol.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
