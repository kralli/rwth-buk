package de.rwth.tm.printer;

import de.rwth.tm.Printer;
import de.rwth.tm.State;
import de.rwth.tm.Symbol;
import de.rwth.tm.Tape;
import de.rwth.tm.TuringMachine;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class FormalPrinter implements Printer {

    @Override
    public void step(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes) {
        Tape tape = tapes[0];

        Tape.Head head = tape.getHeads().get(0);

        int start = Math.min(head.getIndex(), tape.getFirstIndex() - 1);
        int end = Math.max(head.getIndex(), tape.getLastIndex() + 1);
        List<Symbol> symbols = tape.getRange(start, end);

        StringBuilder buffer = new StringBuilder();

        int headIndex = head.getIndex() - start;
        for (int i = 0, length = end - start; i < length; i++) {
            if (i == headIndex) {
                buffer.append('[').append(state).append(']');
            }

            Symbol symbol = symbols.get(i);

            buffer.append(symbol);
        }

        System.out.println(buffer);
    }
}
