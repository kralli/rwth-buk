package de.rwth.tm.printer;

import de.rwth.tm.Printer;
import de.rwth.tm.State;
import de.rwth.tm.Symbol;
import de.rwth.tm.Tape;
import de.rwth.tm.TuringMachine;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class ReadablePrinter implements Printer {

    private final @NotNull List<Snapshot> snapshots = new ArrayList<>();

    @Override
    public void step(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes) {
        snapshots.add(new Snapshot(state, Arrays.asList(tapes)));
    }

    @Override
    public void end(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes) {
        int start = 0;
        int end = 0;
        for (Snapshot snapshot : snapshots) {
            for (TapeSnapshot tape : snapshot.tapes) {
                start = Math.min(tape.tapeOffset, start);
                end = Math.max(tape.tapeOffset + tape.tape.size(), end);
            }
        }

        start -= 2;
        end += 2;

        StringBuilder builder = new StringBuilder();
        for (Snapshot snapshot : snapshots) {
            snapshot.print(builder, start, end - start);
        }
        System.out.println(builder);
    }

    private static final class Snapshot {

        private final @NotNull State state;

        private final TapeSnapshot[] tapes;

        public Snapshot(@NotNull State state, @NotNull List<Tape> tapes) {
            this.state = state;

            this.tapes = tapes.stream().map(TapeSnapshot::new).toArray(TapeSnapshot[]::new);
        }

        public void print(@NotNull StringBuilder buffer, int start, int length) {
            buffer.append("State: ").append(state);
            buffer.append("\n");
            for (TapeSnapshot tape : tapes) {
                tape.print(buffer, start, length);
                buffer.append("\n");
            }
        }
    }

    private static final class TapeSnapshot {

        private final int tapeOffset;
        private final @NotNull List<Symbol> tape;

        private final int[] headIndices;

        public TapeSnapshot(@NotNull Tape tape) {
            this.tapeOffset = tape.getFirstIndex();
            this.tape = tape.getRange(tape.getFirstIndex(), tape.getLastIndex());

            this.headIndices = tape.getHeads().stream().mapToInt(Tape.Head::getIndex).toArray();
        }

        public void print(@NotNull StringBuilder buffer, int start, int length) {
            for (int i = 0, headIndicesLength = headIndices.length; i < headIndicesLength; i++) {
                int index = headIndices[i] - start;

                if (index >= 0 && index < length) {
                    pad(buffer, Math.max(index - 1, 0), ' ');

                    if (index > 0) {
                        buffer.append("[");
                    }
                    buffer.append(i);
                    if (index < length - 1) {
                        buffer.append("]");
                    }
                }
                buffer.append("\n");
            }

            int tapeIndex = tapeOffset - start;
            int tapeLength = Math.min(tape.size(), length - tapeIndex);

            for (int i = 0; i < tapeIndex; i++) {
                buffer.append(Symbol.BLANK);
            }
            for (int i = tapeIndex; i < tapeIndex + tapeLength; i++) {
                buffer.append(tape.get(i - tapeIndex));
            }
            for (int i = tapeIndex + tapeLength; i < length; i++) {
                buffer.append(Symbol.BLANK);
            }
            buffer.append("\n");
        }
    }

    private static void pad(@NotNull StringBuilder buffer, int length, char c) {
        for (int i = 0; i < length; i++) {
            buffer.append(c);
        }
    }
}
