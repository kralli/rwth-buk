package de.rwth.tm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;

public final class Word implements Iterable<Symbol> {

    private final @NotNull List<Symbol> symbols;

    public Word(@NotNull Symbol... symbols) {
        this(Arrays.asList(symbols));
    }

    public Word(@NotNull List<Symbol> symbols) {
        this.symbols = Collections.unmodifiableList(new ArrayList<>(symbols));
    }

    @Override
    public @NotNull Iterator<Symbol> iterator() {
        return symbols.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Word)) {
            return false;
        }
        Word word = (Word) o;
        return symbols.equals(word.symbols);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbols);
    }

    @Override
    public String toString() {
        return symbols.stream().map(Symbol::toString).collect(Collectors.joining(""));
    }
}
