package de.rwth.tm;

import org.jetbrains.annotations.NotNull;

public interface TuringMachine {

    @NotNull States getStates();

    @NotNull State getInitialState();

    @NotNull State getFinalState();

    @NotNull Alphabet getInputAlphabet();

    @NotNull Alphabet getTapeAlphabet();

    @NotNull Word execute(@NotNull Word word, boolean pretty);
}
