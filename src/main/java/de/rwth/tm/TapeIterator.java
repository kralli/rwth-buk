package de.rwth.tm;

import java.util.Iterator;
import org.jetbrains.annotations.NotNull;

interface TapeIterator extends Iterator<Symbol> {

    default boolean hasPrevious() {
        return true;
    }

    @NotNull Symbol previous();

    @Override
    default boolean hasNext() {
        return true;
    }

    @Override
    @NotNull Symbol next();

    @NotNull Symbol get();

    void set(@NotNull Symbol symbol);

    int index();
}