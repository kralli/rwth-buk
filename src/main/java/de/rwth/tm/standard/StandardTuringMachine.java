package de.rwth.tm.standard;

import de.rwth.tm.Alphabet;
import de.rwth.tm.LinkedTape;
import de.rwth.tm.Printer;
import de.rwth.tm.State;
import de.rwth.tm.States;
import de.rwth.tm.Symbol;
import de.rwth.tm.Tape;
import de.rwth.tm.TuringMachine;
import de.rwth.tm.Word;
import de.rwth.tm.printer.FormalPrinter;
import de.rwth.tm.printer.ReadablePrinter;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;

public class StandardTuringMachine implements TuringMachine {

    private final @NotNull States states;
    private final @NotNull State initialState;
    private final @NotNull State finalState;

    private final @NotNull Alphabet inputAlphabet;
    private final @NotNull Alphabet tapeAlphabet;

    private final TransitionFunction transitionFunction;

    public StandardTuringMachine( //
            @NotNull States states, //
            @NotNull Alphabet inputAlphabet, //
            @NotNull Alphabet tapeAlphabet, //
            @NotNull State initialState, //
            @NotNull State finalState, //
            @NotNull TransitionFunction transitionFunction //
    ) {
        if (!states.contains(initialState)) {
            throw new IllegalArgumentException("states must contain initial state: " + initialState + " ∉ " + states);
        }
        if (!states.contains(finalState)) {
            throw new IllegalArgumentException("states must contain final state: " + finalState + " ∉ " + states);
        }

        for (State state : states) {
            for (Symbol symbol : tapeAlphabet) {
                Transition transition;

                try {
                    transition = transitionFunction.transition(state, symbol);
                } catch (Exception exception) {
                    continue;
                }

                if (!states.contains(transition.getState())) {
                    throw new IllegalArgumentException(
                            "transition function specifies unknown state " + transition.getState() + " for (" + state + ", " + symbol + ")");
                }
                if (!tapeAlphabet.contains(transition.getSymbol())) {
                    throw new IllegalArgumentException(
                            "transition function specifies unknown symbol " + transition.getSymbol() + " for (" + state + ", " + symbol + ")");
                }
            }
        }

        if (!tapeAlphabet.contains(inputAlphabet) || inputAlphabet.contains(tapeAlphabet)) {
            throw new IllegalArgumentException("input alphabet is not a true subset of tape alphabet: " + inputAlphabet + " ⊄ " + tapeAlphabet);
        }
        if (!tapeAlphabet.contains(Symbol.BLANK)) {
            throw new IllegalArgumentException("blank is not contained in input alphabet: " + Symbol.BLANK + " ∉ " + tapeAlphabet);
        }

        this.states = states;
        this.initialState = initialState;
        this.finalState = finalState;

        this.inputAlphabet = inputAlphabet;
        this.tapeAlphabet = tapeAlphabet;

        this.transitionFunction = transitionFunction;
    }

    public @NotNull Word execute(@NotNull Word input, boolean pretty) {
        for (Symbol symbol : input) {
            if (!inputAlphabet.contains(symbol)) {
                throw new IllegalArgumentException("symbol in input word is not contained in input alphabet: " + symbol + " ∉ " + inputAlphabet);
            }
        }

        Printer printer = pretty ? new ReadablePrinter() : new FormalPrinter();

        Tape tape = new LinkedTape(tapeAlphabet);
        Tape.Head head = tape.createHead();

        // initialization

        for (Symbol symbol : input) {
            head.write(symbol);
            head.next();
        }

        do {
            head.previous();
        } while (head.read() != Symbol.BLANK);
        head.next();

        // execution

        State state = initialState;

        printer.begin(this, state, tape);

        printer.step(this, state, tape);

        while (!state.equals(finalState)) {
            Symbol symbol = head.read();

            Transition transition = transitionFunction.transition(state, symbol);

            state = transition.getState();
            head.write(transition.getSymbol());

            switch (transition.getDirection()) {
                case LEFT:
                    head.previous();
                    break;
                case RIGHT:
                    head.next();
                    break;
            }

            printer.step(this, state, tape);
        }

        printer.end(this, state, tape);

        // completion

        List<Symbol> output = new ArrayList<>();

        while (inputAlphabet.contains(head.read())) {
            output.add(head.read());
            head.next();
        }

        head.dispose();

        return new Word(output);
    }

    @Override
    public @NotNull States getStates() {
        return states;
    }

    @Override
    public @NotNull State getInitialState() {
        return initialState;
    }

    @Override
    public @NotNull State getFinalState() {
        return finalState;
    }

    @Override
    public @NotNull Alphabet getInputAlphabet() {
        return inputAlphabet;
    }

    @Override
    public @NotNull Alphabet getTapeAlphabet() {
        return tapeAlphabet;
    }
}
