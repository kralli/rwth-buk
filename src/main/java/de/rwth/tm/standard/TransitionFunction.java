package de.rwth.tm.standard;

import de.rwth.tm.State;
import de.rwth.tm.Symbol;
import org.jetbrains.annotations.NotNull;

public interface TransitionFunction {

    @NotNull Transition transition(@NotNull State state, @NotNull Symbol symbol);

}