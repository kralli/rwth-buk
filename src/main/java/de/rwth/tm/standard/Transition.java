package de.rwth.tm.standard;

import de.rwth.tm.Direction;
import de.rwth.tm.State;
import de.rwth.tm.Symbol;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;

public final class Transition {

    private final @NotNull State state;
    private final @NotNull Symbol symbol;
    private final @NotNull Direction direction;

    public Transition(@NotNull State state, @NotNull Symbol symbol, @NotNull Direction direction) {
        this.state = state;
        this.symbol = symbol;
        this.direction = direction;
    }

    public @NotNull State getState() {
        return state;
    }

    public @NotNull Symbol getSymbol() {
        return symbol;
    }

    public @NotNull Direction getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transition)) {
            return false;
        }
        Transition that = (Transition) o;
        return state.equals(that.state) && symbol.equals(that.symbol) && direction == that.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, symbol, direction);
    }

    @Override
    public String toString() {
        return "(" + state + ", " + symbol + ", " + direction + ")";
    }
}