package de.rwth.tm.standard;

import de.rwth.tm.Alphabet;
import de.rwth.tm.Direction;
import de.rwth.tm.State;
import de.rwth.tm.States;
import de.rwth.tm.Symbol;
import de.rwth.tm.TuringMachine;
import de.rwth.tm.parser.Parser;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;

public class StandardParser implements Parser {

    public static @NotNull TuringMachine parse(@NotNull InputStream inputStream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            StandardParser parser = new StandardParser();
            return parser.parse(reader);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public @NotNull TuringMachine parse(@NotNull BufferedReader reader) throws Exception {
        int stateCount = parseInteger(reader);
        Alphabet inputAlphabet = parseAlphabet(reader);
        Alphabet tapeAlphabet = parseAlphabet(reader);
        State initialState = parseState(reader);
        State finalState = parseState(reader);
        Map<Input, Transition> transitions = parseTransitions(reader);

        Set<State> states = transitions.keySet().stream().map(Input::getState).collect(Collectors.toSet());
        states.add(finalState);

        TransitionFunction transitionFunction = (state, symbol) -> {
            Transition transition = transitions.get(new Input(state, symbol));
            if (transition != null) {
                return transition;
            }
            throw new IllegalArgumentException("transition function is undefined for (" + state + ", " + symbol + ")");
        };

        return new StandardTuringMachine(new States(states), inputAlphabet, tapeAlphabet, initialState, finalState, transitionFunction);
    }

    private int parseInteger(@NotNull BufferedReader reader) throws Exception {
        String line = reader.readLine();
        if (line == null) {
            throw new EOFException("expected integer");
        }
        try {
            return Integer.parseInt(line);
        } catch (NumberFormatException e) {
            throw new IOException("malformed integer");
        }
    }

    private @NotNull Alphabet parseAlphabet(@NotNull BufferedReader reader) throws Exception {
        String line = reader.readLine();
        if (line == null) {
            throw new EOFException("expected alphabet");
        }

        Set<Symbol> symbols = new HashSet<>();

        for (char c : line.toCharArray()) {
            Symbol symbol = parseSymbol(String.valueOf(c));

            symbols.add(symbol);
        }

        return new Alphabet(symbols);
    }

    private @NotNull State parseState(@NotNull BufferedReader reader) throws Exception {
        String line = reader.readLine();
        if (line == null) {
            throw new EOFException("expected state");
        }
        return parseState(line);
    }

    private @NotNull Map<Input, Transition> parseTransitions(@NotNull BufferedReader reader) throws Exception {
        Map<Input, Transition> transitions = new HashMap<>();

        String line;
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(" ");

            State inputState = parseState(parts[0]);
            Symbol inputSymbol = parseSymbol(parts[1]);

            State outputState = parseState(parts[2]);
            Symbol outputSymbol = parseSymbol(parts[3]);
            Direction outputDirection = parseDirection(parts[4]);

            Input input = new Input(inputState, inputSymbol);
            Transition output = new Transition(outputState, outputSymbol, outputDirection);

            transitions.put(input, output);
        }

        return transitions;
    }

    private static @NotNull State parseState(@NotNull String value) {
        return State.create(Integer.parseInt(value));
    }

    private static @NotNull Symbol parseSymbol(@NotNull String value) {
        return Symbol.create(value);
    }

    private static @NotNull Direction parseDirection(@NotNull String value) {
        switch (value.toUpperCase(Locale.ROOT)) {
            case "L":
                return Direction.LEFT;
            case "N":
                return Direction.NONE;
            case "R":
                return Direction.RIGHT;
            default:
                throw new IllegalArgumentException("unknown direction: " + value);
        }
    }

    private static final class Input {

        private final @NotNull State state;
        private final @NotNull Symbol symbol;

        private Input(@NotNull State state, @NotNull Symbol symbol) {
            this.state = state;
            this.symbol = symbol;
        }

        public @NotNull State getState() {
            return state;
        }

        public @NotNull Symbol getSymbol() {
            return symbol;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Input)) {
                return false;
            }
            Input input = (Input) o;
            return state.equals(input.state) && symbol.equals(input.symbol);
        }

        @Override
        public int hashCode() {
            return Objects.hash(state, symbol);
        }

        @Override
        public String toString() {
            return "(" + state + ", " + symbol + ")";
        }
    }
}
