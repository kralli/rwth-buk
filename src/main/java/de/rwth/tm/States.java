package de.rwth.tm;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;

public final class States implements Iterable<State> {

    private final @NotNull Set<State> states;

    public States(@NotNull State... states) {
        this(Arrays.asList(states));
    }

    public States(@NotNull Collection<State> states) {
        this.states = Collections.unmodifiableSet(new TreeSet<>(states));
    }

    public boolean contains(@NotNull State symbol) {
        return states.contains(symbol);
    }

    @Override
    public @NotNull Iterator<State> iterator() {
        return states.iterator();
    }

    public int size() {
        return states.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof States)) {
            return false;
        }
        States other = (States) o;
        return states.equals(other.states);
    }

    @Override
    public int hashCode() {
        return Objects.hash(states);
    }

    @Override
    public String toString() {
        return states.stream().map(State::toString).collect(Collectors.joining(", ", "{ ", " }"));
    }
}
