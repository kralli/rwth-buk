package de.rwth.tm;

import java.util.List;
import org.jetbrains.annotations.NotNull;

public interface Tape extends Iterable<Symbol> {

    @NotNull Head createHead();

    @NotNull List<Head> getHeads();

    interface Head {

        void previous();

        void next();

        @NotNull Symbol read();

        void write(@NotNull Symbol symbol);

        int getIndex();

        void dispose();
    }

    int getFirstIndex();

    int getLastIndex();

    int size();

    @NotNull List<Symbol> getRange(int start, int end);

    @Override
    default @NotNull TapeIterator iterator() {
        return iterator(0);
    }

    @NotNull TapeIterator iterator(int index);

}
