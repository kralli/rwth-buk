package de.rwth.tm;

import org.jetbrains.annotations.NotNull;

public interface Printer {

    default void begin(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes) {
    }

    void step(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes);

    default void end(@NotNull TuringMachine turingMachine, @NotNull State state, @NotNull Tape... tapes) {
    }
}
