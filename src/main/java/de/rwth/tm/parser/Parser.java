package de.rwth.tm.parser;

import de.rwth.tm.TuringMachine;
import java.io.BufferedReader;
import org.jetbrains.annotations.NotNull;

public interface Parser {

    @NotNull TuringMachine parse(@NotNull BufferedReader reader) throws Exception;
}
