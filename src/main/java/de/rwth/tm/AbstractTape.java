package de.rwth.tm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractTape implements Tape {

    @Override
    public @NotNull List<Symbol> getRange(int start, int end) {
        if (end < start) {
            throw new IllegalArgumentException("range must not be negative");
        }

        List<Symbol> symbols = new ArrayList<>();

        Iterator<Symbol> iterator = iterator(start);
        for (int i = 0, length = end - start; i < length; i++) {
            Symbol symbol = iterator.next();

            symbols.add(symbol);
        }

        return symbols;
    }

    private final @NotNull List<Head> heads = new ArrayList<>();

    @Override
    public @NotNull Head createHead() {
        HeadImpl head = new HeadImpl(iterator());
        heads.add(head);
        return head;
    }

    @Override
    public @NotNull List<Head> getHeads() {
        return new ArrayList<>(heads);
    }

    public final class HeadImpl implements Tape.Head {

        private @Nullable TapeIterator iterator;

        public HeadImpl(@NotNull TapeIterator iterator) {
            this.iterator = iterator;
        }

        private @NotNull TapeIterator getIterator() {
            if (iterator == null) {
                throw new IllegalStateException("head has already been disposed");
            }
            return iterator;
        }

        public void previous() {
            getIterator().previous();
        }

        public void next() {
            getIterator().next();
        }

        public @NotNull Symbol read() {
            return getIterator().get();
        }

        public void write(@NotNull Symbol symbol) {
            getIterator().set(symbol);
        }

        public int getIndex() {
            return getIterator().index();
        }

        @Override
        public void dispose() {
            iterator = null;
            heads.remove(this);
        }
    }
}
